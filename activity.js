db.users.insertMany([
	{
		"firstName": "Tom",
		"lastName": "Morello",
		"email": "tom.morello@gmail.com",
		"password": "bestsolos123",
		"isAdmin": false
	},
	{
		"firstName": "Brad",
		"lastName": "Wilk",
		"email": "brad.wilk2@gmail.com",
		"password": "drumsolos99",
		"isAdmin": false
	},
	{
		"firstName": "Tim",
		"lastName": "Commerford",
		"email": "tim.commerford1@gmail.com",
		"password": "bassistsAreCool2",
		"isAdmin": false
	},
	{
		"firstName": "Zack",
		"lastName": "De La Rocha",
		"email": "zack.dela.rocha@gmail.com",
		"password": "alwaysAngry9000",
		"isAdmin": false
	},
	{
		"firstName": "Power Theo P",
		"lastName": "Paul",
		"email": "the.fans@gmail.com",
		"password": "weAreOnly4",
		"isAdmin": false
	}
])

db.courses.insertMany([
	{
		"name": "HTML5 Introduction",
		"description": "Basics of HTML5",
		"price": 10000,
		"isActive": true
	},
	{
		"name": "CSS Introduction",
		"description": "Basics of CSS",
		"price": 12000,
		"isActive": true
	},
	{
		"name": "Javascript Introduction",
		"description": "Basics of Javascript",
		"price": 14000,
		"isActive": true
	}
])

db.users.updateOne({},{$set:{"isAdmin": true}})

db.users.find({"isAdmin":true})

db.courses.updateOne({},{$set:{"isActive":false}})


